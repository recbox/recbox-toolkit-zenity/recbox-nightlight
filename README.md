## About:

RecBox Night Light uses Redshift to change color temperature, brightness and gamma in *One-shot* mode. See `man redshift`.

![nightlight](https://gitlab.com/recbox/recbox-screenshots/-/raw/main/recbox-nightlight.gif)

## Installation:

#### Manjaro:
```
git clone https://gitlab.com/recbox/recbox-toolkit-bash/recbox-nightlight.git
cd recbox-nightlight
sudo cp -r usr/* /usr/
sudo gtk-update-icon-cache -f /usr/share/icons/hicolor/
pamac install zenity redshift
```

## Translation
Directory containing translations can be found here:
* /usr/share/recbox-nightlight
* /usr/share/applications/recbox-nightlight.desktop
* /usr/share/applications/recbox-nightlight-settings.desktop

#### Translations:
* [x] en_US
* [x] pl_PL

## Translation contribution:
* type `echo $LANG` in terminal to check language used by your system
* copy `en_US.trans` file and rename it (replace `.utf8` with `.trans`)
* replace text under quotation marks
